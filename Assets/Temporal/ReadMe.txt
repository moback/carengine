copy Assets/* to your project
disable MSAA under 'Project Settings/Quality' (optional)
add the TemporalReprojection component to your cameras
add the VelocityBufferTag component to individual moving meshes (if you want correct motion vectors)
tagging skinned meshes is expensive