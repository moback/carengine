﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class TemporalAAEditor : MonoBehaviour {

	[MenuItem("TemporalAA/SetupCamera")]
	private static void SetupCamera()
	{
		Camera cam = Camera.main;
		if (cam .gameObject.GetComponent<FrustumJitter> () == null)
			cam .gameObject.AddComponent<FrustumJitter> ();
		if (cam .gameObject.GetComponent<VelocityBuffer> () == null)
			cam .gameObject.AddComponent<VelocityBuffer> ();
		if (cam .gameObject.GetComponent<TemporalReprojection> () == null)
			cam .gameObject.AddComponent<TemporalReprojection> ();
	}

	[MenuItem("TemporalAA/DestroyTempAAFromCamera")]
	private static void DestroyTempAAFromCamera()
	{
		Camera cam = Camera.main;
		if (cam .gameObject.GetComponent<TemporalReprojection> () != null)
			DestroyImmediate(cam .gameObject.GetComponent<TemporalReprojection> ());
		if (cam .gameObject.GetComponent<VelocityBuffer> () != null)
			DestroyImmediate(cam .gameObject.GetComponent<VelocityBuffer> ());
		if (cam .gameObject.GetComponent<FrustumJitter> () != null)
			DestroyImmediate(cam .gameObject.GetComponent<FrustumJitter> ());
	}

	[MenuItem("TemporalAA/AddVelBuffTagToObjs")]
	private static void AddVelBuffTagToObjs()
	{
		Transform[] transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			if (transforms [i].gameObject.GetComponent<MeshRenderer> () != null) 
			{
				if (transforms [i].gameObject.GetComponent<VelocityBufferTag> () == null)
					transforms [i].gameObject.AddComponent<VelocityBufferTag> ();
			}
		}
		transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			if (transforms [i].gameObject.GetComponent<Image> () != null) 
			{
				if (transforms [i].gameObject.GetComponent<VelocityBufferTag> () == null)
					transforms [i].gameObject.AddComponent<VelocityBufferTag> ();
			}
		}
	}

	[MenuItem("TemporalAA/AddVelBuffTagToObjsChildren")]
	private static void AddVelBuffTagToObjsChildren()
	{
		Transform[] transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			MeshRenderer[] rend = transforms [i].GetComponentsInChildren<MeshRenderer> ();
			for (int j = 0; j < rend.Length; j++) 
			{
					if (rend[j].gameObject.GetComponent<VelocityBufferTag> () == null)
						rend[j].gameObject.AddComponent<VelocityBufferTag> ();
			}
		}
		transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			Image[] rend = transforms [i].GetComponentsInChildren<Image> ();
			for (int j = 0; j < rend.Length; j++) 
			{
				if (rend[j].gameObject.GetComponent<VelocityBufferTag> () == null)
					rend[j].gameObject.AddComponent<VelocityBufferTag> ();
			}
		}
	}

	[MenuItem("TemporalAA/RemoveVelBuffTagFromObjs")]
	private static void RemoveVelBuffTagFromObjs()
	{
		Transform[] transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			if (transforms [i].gameObject.GetComponent<MeshRenderer> () != null) 
			{
				if (transforms [i].gameObject.GetComponent<VelocityBufferTag> () != null)
					DestroyImmediate(transforms [i].gameObject.GetComponent<VelocityBufferTag>());
			}
		}
		transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			if (transforms [i].gameObject.GetComponent<Image> () != null) 
			{
				if (transforms [i].gameObject.GetComponent<VelocityBufferTag> () != null)
					DestroyImmediate(transforms [i].gameObject.GetComponent<VelocityBufferTag>());
			}
		}
	}

	[MenuItem("TemporalAA/RemoveVelBuffTagFromChildObjs")]
	private static void RemoveVelBuffTagFromChildObjs()
	{
		Transform[] transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			MeshRenderer[] rend = transforms [i].GetComponentsInChildren<MeshRenderer> ();
			for (int j = 0; j < rend.Length; j++) 
			{
				if (rend[j].gameObject.GetComponent<VelocityBufferTag> () != null)
					DestroyImmediate(rend[j].gameObject.GetComponent<VelocityBufferTag> ());
			}
		}
		transforms = Selection.transforms;
		for (int i = 0; i < transforms.Length; i++) 
		{
			Image[] rend = transforms [i].GetComponentsInChildren<Image> ();
			for (int j = 0; j < rend.Length; j++) 
			{
				if (rend[j].gameObject.GetComponent<VelocityBufferTag> () != null)
					DestroyImmediate(rend[j].gameObject.GetComponent<VelocityBufferTag> ());
			}
		}
	}
}