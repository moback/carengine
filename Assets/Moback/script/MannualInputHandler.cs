﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using Valve.VR.InteractionSystem.Sample;
using UnityEngine.UI;

public class MannualInputHandler : MonoBehaviour 
{
	public     GameObject   [] boltGameObject;
	public     string       [] unlockScrewEventText;
	public     AudioClip    [] unlockScrewEventDialog;
	public     AudioSource     screwDialogPlayer;
	public     Text     	   screwDialog; 
	public     int             currentBoltno=0;
	public GameObject		 screwHighlight;



	void Awake()
	{
		//init ();
		//Enablebolt (0);	
	}

 
	IEnumerator Start () 
	{
		yield return  new WaitForSeconds (2);
		//UnityEngine.XR.InputTracking.disablePositionalTracking = true;

	}


	public void OnScrewUnlocked(GameObject obj)
	{
		Destroy (obj.GetComponent<Interactable> ().highlightHolder);
		Destroy(obj);
	}

	public void OnScrewPlacedOnTable()
	{
	}

	public void UnlockScrewEvent(int partno)
	{
	//	Enablebolt (partno);
	//	ShowInUiDialog (partno);
	//	PlayDialog (partno);
//		disablebolt (currentBoltno);
//		currentBoltno++;
	}
	public void HideAllBolts()
	{
		for (int i = 0; i < boltGameObject.Length; i++) 
			boltGameObject[i].SetActive (false);
	}

	void PlayDialog(int dialog)
	{
		screwDialogPlayer.clip = unlockScrewEventDialog [dialog];
		screwDialogPlayer.Play ();
	}

	void ShowInUiDialog(int dialog)
	{
		screwDialog.text = unlockScrewEventText [dialog]; 
	}




 

	void init()
	{
		for (int i = 0; i < boltGameObject.Length; i++) 
			disablebolt (i);
	}
	void Enablebolt(int i)
	{
		boltGameObject [i].GetComponent<Interactable>().enabled =true;
		//boltGameObject [i].GetComponent<CircularDrive>().enabled =true;	
		//boltGameObject [i].GetComponent<MeshCollider>().enabled =true;
	}
	void disablebolt(int i)
	{
		boltGameObject [i].GetComponent<Interactable>().enabled =false;
		//boltGameObject [i].GetComponent<CircularDrive>().enabled =false;	
		//boltGameObject [i].GetComponent<MeshCollider>().enabled =false;
	}
}
