﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Laser : MonoBehaviour
{

	public bool	 active = true;
	public float laserMaxDist = 200;
	public Color colOnHit = new Color(0,1,0,1);
	public Color colOnNotHit = new Color(1,0,0,1);
	public LayerMask laserHitMask;
	public Material mat;

	public LineRenderer lineRenderer = null;
	[HideInInspector]
	public LaserHit laserHitObj = null;
	Hand     hand = null;

	// Use this for initialization
	void Awake () 
	{
		lineRenderer = transform.GetComponent<LineRenderer> ();
		lineRenderer.enabled = false;
		mat = transform.GetComponent<LineRenderer> ().material;
		hand = transform.parent.GetComponent<Hand> ();
	}

	public void Active(bool state)
	{
		active = state;
		if (active)
			lineRenderer.enabled = true;
		else
			lineRenderer.enabled = false;
	}

	// Update is called once per frame
	void Update () 
	{
		if (active && hand.isActive) 
		{
			RaycastHit hitInfo = new RaycastHit ();
			if (Physics.Linecast (transform.position, transform.position + (transform.forward * laserMaxDist), out hitInfo, laserHitMask)) 
			{
				lineRenderer.SetPosition (0, transform.position);
				lineRenderer.SetPosition (1, hitInfo.point);
				if (hitInfo.collider.gameObject.GetComponent<LaserHit> () != null) 
				{
					laserHitObj = hitInfo.collider.gameObject.GetComponent<LaserHit> ();
					if (laserHitObj != null && laserHitObj.enabled) 
					{
						mat.color = colOnHit;
						laserHitObj.hitInfo = hitInfo;
						laserHitObj.laserObj = gameObject;
						laserHitObj.handObj = gameObject.transform.parent.gameObject;
						laserHitObj.OnLaserHit (laserHitObj.hitInfo, laserHitObj.laserObj, laserHitObj.handObj);
					} 
					else
					{
						mat.color = colOnNotHit;
						if (laserHitObj != null)
						{
							laserHitObj.OnLaserNotHit (laserHitObj.hitInfo, laserHitObj.laserObj, laserHitObj.handObj);
							laserHitObj = null;
						}
					}
				} 
				else 
				{
					mat.color = colOnNotHit;
					if (laserHitObj != null) 
					{
						laserHitObj.OnLaserNotHit (laserHitObj.hitInfo, laserHitObj.laserObj, laserHitObj.handObj);
						laserHitObj = null;
					}
				}
			} else {
				mat.color = colOnNotHit;
				lineRenderer.SetPosition (0, transform.position);
				lineRenderer.SetPosition (1, transform.position + (transform.forward * laserMaxDist));
				if (laserHitObj != null) {
					laserHitObj.OnLaserNotHit (laserHitObj.hitInfo, laserHitObj.laserObj, laserHitObj.handObj);
					laserHitObj = null;
				}
			}
		}
	}
}