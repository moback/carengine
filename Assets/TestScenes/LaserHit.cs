﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserHit : MonoBehaviour 
{
	[HideInInspector]
	public RaycastHit hitInfo;
	[HideInInspector]
	public GameObject laserObj;
	[HideInInspector]
	public GameObject handObj;


	public delegate void OnLaserHitDel(RaycastHit hitInfo,GameObject laserObj,GameObject handObj);
	public OnLaserHitDel OnLaserHitEvt;

	public delegate void OnLaserNotHitDel(RaycastHit hitInfo,GameObject laserObj,GameObject handObj);
	public OnLaserNotHitDel OnLaserNotHitEvt;

	public virtual void OnLaserHit(RaycastHit hitInfo,GameObject laserObj,GameObject handObj)
	{
		this.hitInfo = hitInfo;
		this.laserObj = laserObj;
		this.handObj = handObj;
		if(OnLaserHitEvt != null)
			OnLaserHitEvt.Invoke (hitInfo, laserObj, handObj);
	}

	public virtual void OnLaserNotHit(RaycastHit hitInfo,GameObject laserObj,GameObject handObj)
	{
		this.hitInfo = hitInfo;
		this.laserObj = laserObj;
		this.handObj = handObj;
		if(OnLaserHitEvt != null)
			OnLaserNotHitEvt.Invoke(hitInfo, laserObj, handObj);
	}
}