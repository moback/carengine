﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Valve.VR;
using cakeslice;

public class Demo : MonoBehaviour 
{
	[System.Serializable]
	public class StepInfo
	{
		public GameObject 	  headerText;
		public Text			  stepNumText;
		public GameObject 	  taskCompleted;
		public AudioClip  taskCompleteAudio;
		public int stepNum = -1;
		[System.Serializable]
		public class Steps
		{
			[System.Serializable]
			public class BeforePickUp
			{
				public Collider		pickAreaCol;
				public GameObject	objToPickUp;
				public AudioClip	audio;
				public GameObject	effect;
				public GameObject[]	outlineEffect;
				public GameObject	instructions;
			}
			[System.Serializable]
			public class AfterPickUp
			{
				public Collider		placementCollider;
				public Transform	pickscaleTransform;
				public Transform	dropScaleTransform;			
				public AudioClip	audio;
				public GameObject	effect;
				public bool 		hideMeshRenderers;
				public GameObject[]	outlineEffect;
				public GameObject	instructions;
				public Transform	objPlaceTrans;
			}
			public BeforePickUp     beforePickUp;
			public AfterPickUp    	afterPickUp;

		}
		public Steps[] 				steps;
	}

	public StepInfo					stepInfo;

	[System.Serializable]
	public class InteractableObjProp
	{
		public GameObject obj;
		public RaycastHit hitInfo;//The object that is hit
		public GameObject handOwner;//Owner of the hand  for the hitobj	
		public GameObject laserOwner;//Owner of the laser for the hitObj
	}
	private bool 			restartDone;
	private bool 			canPlaceObj;
	public  Color 			hoverColor = Color.green;
	public Color			nonHoverColor = Color.white;
	private bool 			demoEnded;
	public OutlineEffect  outLineEffect;
	public float 		  laserDist;
	public GameObject	demoUi;
	public float		demoStartDelay; 
	private bool 		canStartDemo = false;
	private	bool		trigButTappedForDemoStart = false;
	private bool 		demoStarted = false;
	[HideInInspector]
	private InteractableObjProp leftHandIntractbleObj = null;
	[HideInInspector]
	private InteractableObjProp rightHandIntractbleObj = null;
	private LaserHit[] laserHits = null;
	private bool 	telpButPressed;
	public float 	movSpeed = 1;
	public float	touchPadPosMagForMov = 0.5f;
	private InteractableObjProp	attachedObj = null;
	private Camera  camObj;

	void ExecuteStep()
	{
		stepInfo.stepNum++;
		if (stepInfo.stepNum >= stepInfo.steps.Length) 
		{
			if (stepInfo.taskCompleteAudio != null)
			{
				transform.GetComponent<AudioSource> ().clip = stepInfo.taskCompleteAudio;
				transform.GetComponent<AudioSource> ().Play ();
			}
			demoEnded = true;
			if (stepInfo.taskCompleted != null)
				stepInfo.taskCompleted.gameObject.SetActive (true);
			if (stepInfo.headerText != null)
				stepInfo.headerText.gameObject.SetActive (false);
			Debug.Log ("All Steps Complete");
		}
		else
		{
			for(int j = 0; j < stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect.Length ; j++)
			{
				if(stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect[j] != null)
				{
					for (int k = 0; k < stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect [j].transform.childCount; k++) 
					{
						GameObject outLine = stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect [j].transform.GetChild (k).gameObject;
						if (outLine.GetComponent<cakeslice.Outline>() == null)
							outLine.gameObject.AddComponent<cakeslice.Outline>();
					}
				}
			}
			if (stepInfo.steps [stepInfo.stepNum].beforePickUp.audio != null)
			{
				transform.GetComponent<AudioSource> ().clip = stepInfo.steps [stepInfo.stepNum].beforePickUp.audio;
				transform.GetComponent<AudioSource> ().Play ();
			}
			stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.enabled = true;
		//	stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.GetComponent<LaserHit>().enabled = true;
			if (stepInfo.steps [stepInfo.stepNum].beforePickUp.instructions)
				stepInfo.steps [stepInfo.stepNum].beforePickUp.instructions.gameObject.SetActive(true);
			if (stepInfo.steps [stepInfo.stepNum].beforePickUp.effect != null)
				stepInfo.steps [stepInfo.stepNum].beforePickUp.effect.SetActive (true);
			if (stepInfo.stepNumText != null)
				stepInfo.stepNumText.text = (stepInfo.stepNum + 1).ToString();
		}
	}

	void Update()
	{
		if(demoStarted)
		{
			if(!demoEnded)
			{
				if (attachedObj == null) 
				{
					if (stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.bounds.Contains (InputManager.leftHandObj.transform.position) || stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.bounds.Contains (InputManager.rightHandObj.transform.position))
						outLineEffect.lineColor0 = hoverColor;
					else
						outLineEffect.lineColor0 = nonHoverColor;
				}
				else 
				{
					if (stepInfo.steps [stepInfo.stepNum].afterPickUp.placementCollider.bounds.Contains (attachedObj.handOwner.transform.position))
						outLineEffect.lineColor0 = hoverColor;
					else 
						outLineEffect.lineColor0 = nonHoverColor;
				}	
			}
		}
	}

	// Use this for initialization
	void Start () 
	{
		Screen.fullScreen = true;
	 	leftHandIntractbleObj = null;
		rightHandIntractbleObj = null;
		if(InputManager.leftHandLaser != null)
			InputManager.leftHandLaser.Active(false);
		if(InputManager.rightHandLaser != null)
			InputManager.rightHandLaser.Active(false);
//		laserHits = GameObject.FindObjectsOfType<LaserHit> ();
//		for (int i = 0; i < laserHits.Length; i++) 
//		{
//			laserHits [i].OnLaserHitEvt += OnLaserHit;
//			laserHits [i].OnLaserNotHitEvt += OnLaserNotHit;
//		}
		camObj = Camera.main;
		InputManager.onLeftHandTelprtButtonDownEvt += TelportButPressesed;
		InputManager.onRightHandTelprtButtonDownEvt += TelportButPressesed;

		InputManager.onLeftHandTelprtButtonReleasedEvt += TelportButReleased;
		InputManager.onRightHandTelprtButtonReleasedEvt += TelportButReleased;

		InputManager.LeftHandTouchPosEvt += ProcessMovement;
		InputManager.RightHandTouchPosEvt += ProcessMovement;

		InputManager.onLeftHandTrigButtonPressedEvt += OnLeftHandTrigButtonPressed;
		InputManager.onRightHandTrigButtonPressedEvt += OnRightHandTrigButtonPressed;
		//InputManager.leftHandLaser.lineRenderer.enabled = false;
		//InputManager.rightHandLaser.lineRenderer.enabled = false;
		//InputManager.leftHandLaser.laserMaxDist = 0;
		//InputManager.rightHandLaser.laserMaxDist = 0;
		for (int i = 0; i < stepInfo.steps.Length; i++) 
		{
			for(int j = 0; j < stepInfo.steps [i].beforePickUp.outlineEffect.Length ; j++)
			{
				if(stepInfo.steps [i].beforePickUp.outlineEffect[j] != null)
				{
					cakeslice.Outline outLine = stepInfo.steps [i].beforePickUp.outlineEffect [j].transform.gameObject.GetComponent<cakeslice.Outline>();
					if (outLine != null)
						Destroy (outLine);
					for (int k = 0; k < stepInfo.steps [i].beforePickUp.outlineEffect [j].transform.childCount; k++) 
					{
						outLine = stepInfo.steps [i].beforePickUp.outlineEffect [j].transform.GetChild (k).gameObject.GetComponent<cakeslice.Outline>();
						if (outLine != null)
							Destroy (outLine);
					}
				}
			}
			stepInfo.steps [i].beforePickUp.pickAreaCol.enabled = false;
			//stepInfo.steps [i].beforePickUp.pickAreaCol.GetComponent<LaserHit>().enabled = false;
			if(stepInfo.steps [i].beforePickUp.effect != null)
				stepInfo.steps [i].beforePickUp.effect.SetActive (false);
			if(stepInfo.steps [i].beforePickUp.instructions != null)
				stepInfo.steps [i].beforePickUp.instructions.gameObject.SetActive (false);


			for(int j = 0; j < stepInfo.steps [i].afterPickUp.outlineEffect.Length ; j++)
			{
				if(stepInfo.steps [i].afterPickUp.outlineEffect[j] != null)
				{
					cakeslice.Outline outLine = stepInfo.steps [i].afterPickUp.outlineEffect [j].transform.gameObject.GetComponent<cakeslice.Outline> ();
					if (outLine != null)
						Destroy (outLine);
					for (int k = 0; k < stepInfo.steps [i].afterPickUp.outlineEffect [j].transform.childCount; k++) 
					{
						outLine = stepInfo.steps [i].afterPickUp.outlineEffect [j].transform.GetChild (k).gameObject.GetComponent<cakeslice.Outline>();
						if (outLine != null)
							Destroy (outLine);
					}
				}
			}
			stepInfo.steps [i].afterPickUp.placementCollider.enabled = false;
			if(stepInfo.steps [i].afterPickUp.effect != null)
				stepInfo.steps [i].afterPickUp.effect.SetActive (false);
			if(stepInfo.steps [i].afterPickUp.instructions != null)
				stepInfo.steps [i].afterPickUp.instructions.gameObject.SetActive (false);
		}
		//ExecuteStep ();
	}

	void DoPlacementSetUpAfterObjectPickUp(InteractableObjProp attachedObj)
	{
		//attachedObj.laserOwner.GetComponent<Laser> ().laserHitObj = null;
		//attachedObj.laserOwner.GetComponent<Laser> ().Active (false);
		attachedObj.obj.transform.parent = attachedObj.handOwner.transform;
		attachedObj.obj.transform.position = attachedObj.handOwner.transform.Find("Laser").transform.position;
		if (stepInfo.steps [stepInfo.stepNum].afterPickUp.pickscaleTransform != null)
			attachedObj.obj.transform.localScale = stepInfo.steps [stepInfo.stepNum].afterPickUp.pickscaleTransform.transform.lossyScale;
		for(int j = 0; j < stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect.Length ; j++)
		{
			if(stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect[j] != null)
			{
				cakeslice.Outline outLine = stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect [j].transform.gameObject.GetComponent<cakeslice.Outline> ();
				if (outLine != null)
					Destroy(outLine);
				for (int k = 0; k < stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect [j].transform.childCount; k++) 
				{
					outLine = stepInfo.steps [stepInfo.stepNum].beforePickUp.outlineEffect [j].transform.GetChild (k).gameObject.GetComponent<cakeslice.Outline>();
					if (outLine != null)
						Destroy(outLine);
				}
			}
		}
		//stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.GetComponent<LaserHit>().enabled = false;
		if (stepInfo.steps [stepInfo.stepNum].beforePickUp.effect != null)
			stepInfo.steps [stepInfo.stepNum].beforePickUp.effect.gameObject.SetActive (false);
		if (stepInfo.steps [stepInfo.stepNum].beforePickUp.instructions != null)
			stepInfo.steps [stepInfo.stepNum].beforePickUp.instructions.gameObject.SetActive (false);

		for(int j = 0; j < stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect.Length ; j++)
		{
			if(stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect[j] != null)
			{
				GameObject outLine = stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.gameObject;
				if (outLine.GetComponent<cakeslice.Outline> () == null) 
				{
					if (stepInfo.steps [stepInfo.stepNum].afterPickUp.hideMeshRenderers) {
						if (stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].GetComponent<MeshRenderer> () != null)
							stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].GetComponent<MeshRenderer> ().enabled = false;
					}
					outLine.AddComponent<cakeslice.Outline> ();
				}
				for (int k = 0; k < stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.childCount; k++) 
				{
					outLine = stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.GetChild (k).gameObject;
					if (outLine.GetComponent<cakeslice.Outline> () == null)
					{
						if (stepInfo.steps [stepInfo.stepNum].afterPickUp.hideMeshRenderers) {
							if (stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].GetComponent<MeshRenderer> () != null)
								stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].GetComponent<MeshRenderer> ().enabled = false;
						}
						outLine.AddComponent<cakeslice.Outline> ();
					}
				}
			}
		}
		if (stepInfo.steps [stepInfo.stepNum].afterPickUp.audio != null)
		{
			transform.GetComponent<AudioSource> ().clip = stepInfo.steps [stepInfo.stepNum].afterPickUp.audio;
			transform.GetComponent<AudioSource> ().Play ();
		}
		stepInfo.steps [stepInfo.stepNum].afterPickUp.placementCollider.enabled = true;
		//stepInfo.steps [stepInfo.stepNum].afterPickUp.placementCollider.GetComponent<LaserHit> ().enabled = true;
		if (stepInfo.steps [stepInfo.stepNum].afterPickUp.effect != null)
			stepInfo.steps [stepInfo.stepNum].afterPickUp.effect.gameObject.SetActive (true);
		if (stepInfo.steps [stepInfo.stepNum].afterPickUp.instructions != null)
			stepInfo.steps [stepInfo.stepNum].afterPickUp.instructions.gameObject.SetActive (true);

		leftHandIntractbleObj = null;
		rightHandIntractbleObj = null;
	}

	void OnLeftHandTrigButtonPressed()
	{
		if (demoEnded && !restartDone) {
			restartDone = true;
			UnsubscribeEvts ();
			SceneManager.LoadScene (0);
			return;
		}
		if (!demoStarted && !canStartDemo)
		{
			canStartDemo = true;
			if(demoUi != null)
				demoUi.SetActive (false);
			Invoke ("StartDemo", demoStartDelay);
		}
		if (demoStarted) 
		{
			if (leftHandIntractbleObj != null) 
			{
				attachedObj = new InteractableObjProp (); 
				//attachedObj.hitInfo = leftHandIntractbleObj.hitInfo;
				attachedObj.obj = stepInfo.steps [stepInfo.stepNum].beforePickUp.objToPickUp;
				attachedObj.handOwner = leftHandIntractbleObj.handOwner;
				attachedObj.laserOwner = leftHandIntractbleObj.laserOwner;
				DoPlacementSetUpAfterObjectPickUp (attachedObj);
				return;
			}
			TriggerButtonPressedForPlacement (InputManager.leftHandObj.gameObject);
		}
	}

	void StartDemo()
	{
		ExecuteStep ();

		//InputManager.leftHandLaser.laserMaxDist = laserDist;
		//InputManager.rightHandLaser.laserMaxDist = laserDist;
		//InputManager.leftHandLaser.Active(true);
		//InputManager.rightHandLaser.Active(true);

		demoStarted = true;
	}

	void UnsubscribeEvts()
	{
		InputManager.onLeftHandTelprtButtonDownEvt -= TelportButPressesed;
		InputManager.onRightHandTelprtButtonDownEvt -= TelportButPressesed;

		InputManager.onLeftHandTelprtButtonReleasedEvt -= TelportButReleased;
		InputManager.onRightHandTelprtButtonReleasedEvt -= TelportButReleased;

		InputManager.LeftHandTouchPosEvt -= ProcessMovement;
		InputManager.RightHandTouchPosEvt -= ProcessMovement;

		InputManager.onLeftHandTrigButtonPressedEvt -= OnLeftHandTrigButtonPressed;
		InputManager.onRightHandTrigButtonPressedEvt -= OnRightHandTrigButtonPressed;
	}

	void OnRightHandTrigButtonPressed()
	{
		if (demoEnded && !restartDone)
		{
			restartDone = true;
			UnsubscribeEvts ();
			SceneManager.LoadScene (0);
			return;
		}
		if (!demoStarted && !canStartDemo)
		{
			canStartDemo = true;
			if(demoUi != null)
				demoUi.SetActive (false);
			Invoke ("StartDemo", demoStartDelay);
		}
		if (demoStarted) 
		{
			if (rightHandIntractbleObj != null) 
			{
				attachedObj = new InteractableObjProp (); 
				attachedObj.obj = stepInfo.steps [stepInfo.stepNum].beforePickUp.objToPickUp;
				//attachedObj.hitInfo = rightHandIntractbleObj.hitInfo;
				attachedObj.handOwner = rightHandIntractbleObj.handOwner;
				attachedObj.laserOwner = rightHandIntractbleObj.laserOwner;
				DoPlacementSetUpAfterObjectPickUp (attachedObj);
				return;
			}
			TriggerButtonPressedForPlacement (InputManager.rightHandObj.gameObject);
		}
	}

	void TriggerButtonPressedForPlacement(GameObject handObj)
	{
		if (attachedObj == null) 
		{
			if (GameObject.Equals (InputManager.leftHandObj.gameObject,handObj) && stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.bounds.Contains (InputManager.leftHandObj.gameObject.transform.position)) 
			{
				attachedObj = new InteractableObjProp (); 
				attachedObj.obj = stepInfo.steps [stepInfo.stepNum].beforePickUp.objToPickUp;
				//attachedObj.hitInfo = stepInfo.steps[stepInfo.stepNum].beforePickUp.objToPickUp;
				attachedObj.handOwner = InputManager.leftHandObj.gameObject;
				//attachedObj.laserOwner = leftHandIntractbleObj.laserOwner;
				DoPlacementSetUpAfterObjectPickUp (attachedObj);
			}
			else if (GameObject.Equals (InputManager.rightHandObj.gameObject,handObj) && stepInfo.steps [stepInfo.stepNum].beforePickUp.pickAreaCol.bounds.Contains (InputManager.rightHandObj.gameObject.transform.position)) 
			{
				attachedObj = new InteractableObjProp (); 
				attachedObj.obj = stepInfo.steps[stepInfo.stepNum].beforePickUp.objToPickUp.gameObject;
				attachedObj.handOwner = InputManager.rightHandObj.gameObject;
				//attachedObj.laserOwner = rightHandIntractbleObj.laserOwner;
				DoPlacementSetUpAfterObjectPickUp (attachedObj);
			}
		}
		else if (attachedObj != null && GameObject.Equals(attachedObj.handOwner,handObj) && stepInfo.steps [stepInfo.stepNum].afterPickUp.placementCollider.bounds.Contains(handObj.transform.position)) 
		{
			attachedObj.obj.transform.parent = null;
			//attachedObj.laserOwner.GetComponent<Laser> ().laserHitObj = null;
			//attachedObj.laserOwner.GetComponent<Laser> ().Active (true);
			for(int j = 0; j < stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect.Length ; j++)
			{
				if(stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect[j] != null)
				{
					cakeslice.Outline outLine = stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.gameObject.GetComponent<cakeslice.Outline>();
					if (outLine != null)
						Destroy(outLine);
					for (int k = 0; k < stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.childCount; k++) 
					{
						outLine = stepInfo.steps [stepInfo.stepNum].afterPickUp.outlineEffect [j].transform.GetChild (k).gameObject.GetComponent<cakeslice.Outline>();
						if (outLine != null)
							Destroy(outLine);
					}
				}
			}
			stepInfo.steps [stepInfo.stepNum].afterPickUp.placementCollider.enabled = false;
			if (stepInfo.steps [stepInfo.stepNum].afterPickUp.effect != null)
				stepInfo.steps [stepInfo.stepNum].afterPickUp.effect.gameObject.SetActive (false);
			if (stepInfo.steps [stepInfo.stepNum].afterPickUp.instructions != null)
				stepInfo.steps [stepInfo.stepNum].afterPickUp.instructions.gameObject.SetActive (false);
			Transform trans = stepInfo.steps [stepInfo.stepNum].afterPickUp.objPlaceTrans;
			attachedObj.obj.transform.position = trans.position;
			attachedObj.obj.transform.rotation = trans.rotation;
			if (stepInfo.steps [stepInfo.stepNum].afterPickUp.dropScaleTransform != null)
				attachedObj.obj.transform.localScale = stepInfo.steps [stepInfo.stepNum].afterPickUp.dropScaleTransform.transform.lossyScale;
			
			attachedObj = null;
			ExecuteStep ();
		}
	}

	void OnLaserHit(RaycastHit hitInfo,GameObject laserObj,GameObject handObj)
	{
		if (attachedObj == null) 
		{
			if (GameObject.Equals (handObj, InputManager.leftHandObj.gameObject)) 
			{
				leftHandIntractbleObj = new InteractableObjProp ();
				leftHandIntractbleObj.hitInfo = hitInfo;
				leftHandIntractbleObj.laserOwner = laserObj;
				leftHandIntractbleObj.handOwner = handObj;
			}
			else if (GameObject.Equals (handObj, InputManager.rightHandObj.gameObject))
			{
				rightHandIntractbleObj = new InteractableObjProp ();
				rightHandIntractbleObj.hitInfo = hitInfo;
				rightHandIntractbleObj.laserOwner = laserObj;
				rightHandIntractbleObj.handOwner = handObj;
			}
		}
	}

	void OnLaserNotHit(RaycastHit hitInfo,GameObject laserObj,GameObject handObj)
	{
		if (attachedObj == null) 
		{
			if (GameObject.Equals (handObj, InputManager.leftHandObj.gameObject))
				leftHandIntractbleObj = null;
			else if (GameObject.Equals (handObj, InputManager.rightHandObj.gameObject))
				rightHandIntractbleObj = null;
		} 
	}

	void TelportButReleased()
	{
		telpButPressed = false;
	}

	void TelportButPressesed()
	{
		telpButPressed = true;
	}

	void ProcessMovement(Vector2 pos)
	{
		if (demoStarted) 
		{
			if (telpButPressed && pos.magnitude >= touchPadPosMagForMov) 
			{
				Vector3 dirToMov = Vector3.zero;
				if (Vector2.Angle (Vector2.right, pos) <= 45) 
				{
					dirToMov = Vector3.ProjectOnPlane (camObj.transform.right, Vector3.up);
					dirToMov.Normalize ();
					transform.position = transform.position + (dirToMov * (movSpeed * Time.deltaTime));
				} 
				else if (Vector2.Angle (-Vector2.right, pos) <= 45) 
				{
					dirToMov = Vector3.ProjectOnPlane (-camObj.transform.right, Vector3.up);
					dirToMov.Normalize ();
					transform.position = transform.position + (dirToMov * (movSpeed * Time.deltaTime));
				}
				else if (Vector2.Angle (Vector2.up, pos) <= 45) 
				{
					dirToMov = Vector3.ProjectOnPlane (camObj.transform.forward, Vector3.up);
					dirToMov.Normalize ();
					transform.position = transform.position + (dirToMov * (movSpeed * Time.deltaTime));
				} 
				else if (Vector2.Angle (-Vector2.up, pos) <= 45) 
				{
					dirToMov = Vector3.ProjectOnPlane (-camObj.transform.forward, Vector3.up);
					dirToMov.Normalize ();
					transform.position = transform.position + (dirToMov * (movSpeed * Time.deltaTime));
				}
			}
		}
	}
}